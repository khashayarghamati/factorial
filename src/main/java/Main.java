public class Main {

    public static void main(String[] args){
        long startTime = System.nanoTime();
        String answer = MembersHolder.factorial.calculate_factorial(90000);

        System.out.printf("Time: %d ms%n", (System.nanoTime() - startTime) / 1000000);
        System.out.printf("Answer is %S", answer);
    }
}
