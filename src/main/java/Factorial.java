import com.google.common.math.BigIntegerMath;

final class Factorial {


    static String calculate_factorial(int number){
        return BigIntegerMath.factorial(number).toString();
    }

}
